import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';
import { Branch } from './branch/entities/branch.entity';
import { BranchModule } from './branch/branch.module';
import { Stock } from './stocks/entities/stock.entity';
import { StocksModule } from './stocks/stocks.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { BuyStock } from './buystocks/entities/buyStock.entity';
import { BuyStocksModule } from './buystocks/buyStocks.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [User, Member, Branch, Stock, BuyStock],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TemperatureModule,
    UsersModule,
    MembersModule,
    BranchModule,
    StocksModule,
    BuyStocksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
