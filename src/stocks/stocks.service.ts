import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/Stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private StocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    stock.name = createStockDto.name;
    stock.min = parseInt(createStockDto.min);
    stock.balance = parseInt(createStockDto.balance);
    stock.unit = createStockDto.unit;
    stock.status = createStockDto.status;
    stock.branch = JSON.parse(createStockDto.branchs);
    if (createStockDto.image && createStockDto.image !== '') {
      stock.image = createStockDto.image;
    }
    return this.StocksRepository.save(stock);
  }
  findAll() {
    return this.StocksRepository.find();
  }

  findOne(id: number) {
    return this.StocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.StocksRepository.findOneOrFail({
      where: { id },
    });
    stock.name = updateStockDto.name;
    stock.min = parseFloat(updateStockDto.min);
    stock.balance = parseFloat(updateStockDto.balance);
    stock.unit = updateStockDto.unit;
    stock.status = updateStockDto.status;
    stock.branch = JSON.parse(updateStockDto.branchs);
    if (updateStockDto.image && updateStockDto.image !== '') {
      stock.image = updateStockDto.image;
    }
    this.StocksRepository.save(stock);
    const result = await this.StocksRepository.findOne({
      where: { id },
      relations: { branch: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.StocksRepository.findOneOrFail({
      where: { id },
    });
    await this.StocksRepository.remove(deleteStock);
    return deleteStock;
  }
}
