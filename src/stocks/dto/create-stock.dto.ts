import { IsNotEmpty } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  min: string;
  @IsNotEmpty()
  balance: string;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  status: 'Available' | 'Low' | 'Out of Stock';

  image: string;

  branchs: string;
}
