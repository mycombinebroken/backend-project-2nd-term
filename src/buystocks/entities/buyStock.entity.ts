import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BuyStock {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  balance: number;
  @Column()
  unit: string;
  @Column()
  cost: number;
}
