import { Injectable } from '@nestjs/common';
import { CreateBuyStockDto } from './dto/create-buyStock.dto';
import { UpdateBuyStockDto } from './dto/update-buyStock.dto';
import { BuyStock } from './entities/buyStock.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BuyStocksService {
  // lastId: number = 5;
  // buyStocks: BuyStock[] = [
  //   {
  //     id: 1,
  //     email: 'test',
  //     password: '1111',
  //     fullName: 'Kritkhanin Anantakul',
  //     gender: 'male',
  //     // roles: ['admin'],
  //   },
  //   {
  //     id: 2,
  //     email: 'Wittaya@gmail.com',
  //     password: '1234',
  //     fullName: 'Wittaya Judnguhlearm',
  //     gender: 'male',
  //     // roles: ['buyStock'],
  //   },
  //   {
  //     id: 3,
  //     email: 'Pasinee@gmail.com',
  //     password: '1234',
  //     fullName: 'Pasinee Chaweenat',
  //     gender: 'female',
  //     // roles: ['buyStock'],
  //   },
  //   {
  //     id: 4,
  //     email: 'Jedsada@gmail.com',
  //     password: '1234',
  //     fullName: 'Jedsada Nathee',
  //     gender: 'male',
  //     // roles: ['buyStock'],
  //   },
  //   {
  //     id: 5,
  //     email: 'Arthaphan@gmail.com',
  //     password: '1234',
  //     fullName: 'Arthaphan Charoenchaisakulsuk',
  //     gender: 'male',
  //     // roles: ['buyStock'],
  //   },
  // ];
  // create(createBuyStockDto: CreateBuyStockDto) {
  //   this.lastId++;
  //   const newBuyStock = { ...createBuyStockDto, id: this.lastId };
  //   this.buyStocks.push(newBuyStock);
  //   return newBuyStock;
  // }
  constructor(
    @InjectRepository(BuyStock)
    private buyStocksRepository: Repository<BuyStock>,
  ) {}

  create(createBuyStockDto: CreateBuyStockDto): Promise<BuyStock> {
    return this.buyStocksRepository.save(createBuyStockDto);
  }

  findAll(): Promise<BuyStock[]> {
    return this.buyStocksRepository.find();
  }

  findOne(id: number) {
    return this.buyStocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateBuyStockDto: UpdateBuyStockDto) {
    await this.buyStocksRepository.update(id, updateBuyStockDto);
    const buyStock = await this.buyStocksRepository.findOneBy({ id });
    return buyStock;
  }

  async remove(id: number) {
    const deleteBuyStock = await this.buyStocksRepository.findOneBy({ id });
    return this.buyStocksRepository.remove(deleteBuyStock);
  }
}
