import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  // lastId: number = 5;
  // users: User[] = [
  //   {
  //     id: 1,
  //     email: 'test',
  //     password: '1111',
  //     fullName: 'Kritkhanin Anantakul',
  //     gender: 'male',
  //     // roles: ['admin'],
  //   },
  //   {
  //     id: 2,
  //     email: 'Wittaya@gmail.com',
  //     password: '1234',
  //     fullName: 'Wittaya Judnguhlearm',
  //     gender: 'male',
  //     // roles: ['user'],
  //   },
  //   {
  //     id: 3,
  //     email: 'Pasinee@gmail.com',
  //     password: '1234',
  //     fullName: 'Pasinee Chaweenat',
  //     gender: 'female',
  //     // roles: ['user'],
  //   },
  //   {
  //     id: 4,
  //     email: 'Jedsada@gmail.com',
  //     password: '1234',
  //     fullName: 'Jedsada Nathee',
  //     gender: 'male',
  //     // roles: ['user'],
  //   },
  //   {
  //     id: 5,
  //     email: 'Arthaphan@gmail.com',
  //     password: '1234',
  //     fullName: 'Arthaphan Charoenchaisakulsuk',
  //     gender: 'male',
  //     // roles: ['user'],
  //   },
  // ];
  // create(createUserDto: CreateUserDto) {
  //   this.lastId++;
  //   const newUser = { ...createUserDto, id: this.lastId };
  //   this.users.push(newUser);
  //   return newUser;
  // }
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto): Promise<User> {
    return this.usersRepository.save(createUserDto);
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.usersRepository.update(id, updateUserDto);
    const user = await this.usersRepository.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneBy({ id });
    return this.usersRepository.remove(deleteUser);
  }
}
